<?php
session_start();
if(isset($_SESSION['user'])) {
    header('Location: /index.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Нарушениям.нет</title>
    <link rel="stylesheet" href="assets/style/style.css">
</head>
<body>
    <header>
        <div class="logo">
            <div class="logopic"><img src="assets/images/logo.jpg" ></div>
            <div class="logotext"><a href="index.php">Нарушениям.нет</a></div>
        </div>
        <div class="menu">
            <div class="login">
                <a href="login.php"><p>Вход</p></a>
            </div>
            <div class="registr">
                <a href="reg.php"><p>Регистрация</p></a>
            </div>
        </div>
    </header>
    <main>
        <h2>Регистрация</h2>
        <form action="vendor/auth/login.php" method="post" class="form">
            <div class="formtitle"></div>
            <label for="login">Логин</label>
            <input type="text" class="form-input" name="login" required>
            <label for="pass">Пароль</label>
            <input type="password" class="form-input" name="pass" required>

            <button class="appcreate form-btn">Войти в аккаунт</button>
        </form>
        <?php

            if (isset($_SESSION['message'])) {
        ?>
            <div class="message <?= $_SESSION['message']['type'] ?>">
                <p class="message-text"><?= $_SESSION['message']['text'] ?></p>
            </div>
        <?php
            }
        ?>
    </main>
    <footer>
        <span>©️ Нарушениям.нет 2024 г. Все права защищены.</span>
    </footer>
</body>
</html>