<?php 
session_start();

require_once 'vendor/db.php';

if (!isset($_SESSION['user'])) {
    header('Location: /index.php');
}
$user_id = $_SESSION['user']['id'];
$query = "SELECT * FROM `applications` WHERE `user_id` = '$user_id'";
$responce = mysqli_query($db, $query);
$apps = mysqli_fetch_all($responce, MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Нарушениям.нет</title>
    <link rel="stylesheet" href="assets/style/style.css">
</head>
<body>
    <header>
        <div class="logo">
            <div class="logopic"><img src="assets/images/logo.jpg" ></div>
            <div class="logotext"><a href="index.php">Нарушениям.нет</a></div>
        </div>
        <div class="menu">
            <div>
                <a href="acc.php"><p><?php $_SESSION['user']['login'] ?></p></a>
            </div>
            <div>
                <a href="vendor/auth/logout.php"><p>Выход</p></a>
            </div>
        </div>
    </header>
    <main>
         <h2>Мои заявления</h2>
         <?php

            if (isset($_SESSION['message'])) {
        ?>
            <div class="message <?= $_SESSION['message']['type'] ?>">
                <p class="message-text"><?= $_SESSION['message']['text'] ?></p>
            </div>
        <?php
            }
        ?>
         <center><table>
            <tr><th>Номер</th><th style="width: 60%;">Описание</th><th>Статус</th><th >Управление</th></tr>
            <?php 
            foreach ($apps as $app) {?>
                <tr><td><?= $app['number']?></td><td><?= $app['description']?></td><td><?= $app['status']?></td><td style="display :flex; flex-direction: row; justify-content: space-between">
                    <?php 
                        if ($app['status'] == 'На рассмотрении') {
                        ?>
                        <a href="editapp.php?id=<?= $app['id']?>">Редактировать</a><a href="deleteapp.php?id=<?= $app['id']?>">Удалить</a>
                    <?
                        } else {
                            ?> 
                                <p>Вы не можете управлять рассмотренным заявлением</p>
                            <?
                        }
                    ?>    
                </td></tr>
                <?
            }
            ?>
        </table></center>
    </main>
    <footer>
        <span>©️ Нарушениям.нет 2024 г. Все права защищены.</span>
    </footer>
</body>
</html>