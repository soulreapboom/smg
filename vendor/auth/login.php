<?php

session_start();
require_once '../db.php';

$login = $_POST['login'];
$pass = $_POST['pass'];

$query = "SELECT * FROM `users` WHERE (`login` = '$login' AND `password` = '$pass') LIMIT 1";
$result = mysqli_query($db, $query);
if (mysqli_num_rows($result) === 0) {
    $_SESSION['message'] = [
        'type' => 'error',
        'text'=> 'Неверный логин или пароль'
    ];
    header('Location: /login.php');
    die();
} else {
    $result = mysqli_fetch_array($result);
    $_SESSION['user'] = [
        'id' => $result['id'],
        'login'=> $result['login'],
        'level' => $result['level'],
        'password' => $result['password'],
        'fio' => $result['fio'],
        'email' => $result['email'],
        'phone' => $result['phone'],
    ];

    unset($_SESSION['message']);
    unset($_SESSION['validation']);

    header('Location: /index.php');
}
?>