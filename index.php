<?php 
session_start();

require_once 'vendor/db.php';

$query = "SELECT * FROM `applications`";
$responce = mysqli_query($db, $query);
$apps = mysqli_fetch_all($responce, MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Нарушениям.нет</title>
    <link rel="stylesheet" href="assets/style/style.css">
</head>
<body>
    <header>
        <div class="logo">
            <div class="logopic"><img src="assets/images/logo.jpg" ></div>
            <div class="logotext"><a href="index.php">Нарушениям.нет</a></div>
        </div>
        <div class="menu">
            <?php 
                if (isset($_SESSION['user'])){
                    if ($_SESSION['user']['level'] == '2'){
                        echo '
                            <div>
                                <a href="admin/index.php"><p>Админ панель</p></a>
                            </div>
                            ';
                    } else {
                        echo '
                            <div>
                                <a href="acc.php"><p>' . $_SESSION['user']['login'] . '</p></a>
                            </div>
                            ';
                    }
                    echo '
                        <div>
                            <a href="vendor/auth/logout.php"><p>Выход</p></a>
                        </div>
                        ';
                } else {
                    echo '
                        <div class="login">
                            <a href="login.php"><p>Вход</p></a>
                        </div>
                        <div class="registr">
                            <a href="reg.php"><p>Регистрация</p></a>
                        </div>
                        ';
                }
            ?>
           
        </div>
    </header>
    <main>
        <h2>Заявления</h2>
        <div class="appblock">
            <?php
            foreach ($apps as $app) {
                ?>
                <div class="app">
                <div class="apptitle">
                    <h3>Номер нарушителя</h3>
                    <p><?= $app['number']?></p>
                </div>
                <div class="desc">
                    <h3>Описание нарушения</h3>
                    <p><?= $app['description']?></p>
                </div>
                <div class="status">
                    <h3>Статус заявления</h3>
                    <p class="<?php
                    if ($app['status'] == 'Отклонено') {
                        echo 'canceled';
                    } else if ($app['status'] == 'Решено') {
                        echo 'solved';
                    }
                ?>">
                <?= $app['status']?></p>
                </div>
                </div>
            <?
            }
            ?>
        </div>

        <center><a href="createapp.php"><button class="appcreate">Создать заявление</button></a></center>
    </main>
    <footer>
        <span>©️ Нарушениям.нет 2024 г. Все права защищены.</span>
    </footer>
</body>
</html>