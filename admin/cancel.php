<?php 
session_start();

require_once '../vendor/db.php';

if(!isset($_SESSION['user'])) {
    header('Location: /login.php');
}
$id = $_GET['id'];
$query = "SELECT * FROM `applications` WHERE `id` = '$id'";
$response = mysqli_query($db, $query);
$app = mysqli_fetch_assoc($response);
?>

<!DOCTYPE html>
<html lang="en">
<head>  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Нарушениям.нет</title>
    <link rel="stylesheet" href="../assets/style/style.css">
</head>
<body>
    <header>
        <div class="logo">
            <div class="logopic"><img src="../assets/images/logo.jpg" ></div>
            <div class="logotext"><a href="../index.php">Нарушениям.нет</a></div>
        </div>
        <div class="menu">
            <div>
                <a href="/index.php"><p>Админ панель</p></a>
            </div>
            <div>
                <a href="vendor/auth/logout.php"><p>Выход</p></a>
            </div>
        </div>
    </header>
    <main>
        <h2>Отклонить заявление</h2>
        <center><form action="../vendor/admin/cancelapp.php?id=<?php printf($app['id'])?>" method="post">
            <div class="app createapp">
                <div class="apptitle createapptitle">
                    <h3>Номер нарушителя</h3>
                    <p><?php printf($app['number']) ?></p>
                </div>
                <div class="desc createappdesc">
                    <h3>Описание нарушения</h3>
                    <p><?php printf($app['description']) ?>
                </div>
                <button class="appcreate form-btn">Отклонить заявление</button>
            </div>
        </form></center>
        <?php

            if (isset($_SESSION['message'])) {
        ?>
            <div class="message <?= $_SESSION['message']['type'] ?>">
                <p class="message-text"><?= $_SESSION['message']['text'] ?></p>
            </div>
        <?php
            }
        ?>
    </main>
    <footer>
        <span>©️ Нарушениям.нет 2024 г. Все права защищены.</span>
    </footer>
</body>
</html>