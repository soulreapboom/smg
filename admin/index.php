<?php 
session_start();

require_once '../vendor/db.php';

if (!isset($_SESSION['user']) || $_SESSION['user']['level'] != 2){
    header('Location: ../index.php');
}
$query = "SELECT * FROM `applications`";
$responce = mysqli_query($db, $query);
$apps = mysqli_fetch_all($responce, MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Нарушениям.нет</title>
    <link rel="stylesheet" href="../assets/style/style.css">
</head>
<body>
    <header>
        <div class="logo">
            <div class="logopic"><img src="../assets/images/logo.jpg" ></div>
            <div class="logotext"><a href="../index.php">Нарушениям.нет</a></div>
        </div>
        <div class="menu">
            <div>
                <a href="index.php"><p>Админ панель</p></a>
            </div>
            <div>
                <a href="../vendor/auth/logout.php"><p>Выход</p></a>
            </div>
        </div>
    </header>
    <main>
         <h2>Все заявления</h2>
         <center><table>
            <tr><th>id</th><th>Номер</th><th style="width: 60%;">Описание</th><th>Статус</th><th>Управление</th></tr>
            <?php 
            foreach ($apps as $app) {?>
                <tr><td><?= $app['id']?></td><td><?= $app['number']?></td><td><?= $app['description']?></td><td><?= $app['status']?></td><td style="display :flex; flex-direction: row; justify-content: space-between"><a href="solve.php?id=<?=$app['id']?>">Решить</a><a href="cancel.php?id=<?=$app['id']?>">Отклонить</a></td></tr>
                <?
            }
            ?>
        </table></center>
    </main>
    <footer>
        <span>©️ Нарушениям.нет 2024 г. Все права защищены.</span>
    </footer>
</body>
</html>