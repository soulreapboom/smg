<?php 

session_start();

require_once '../db.php';

$id = $_GET['id'];
$query = "SELECT * FROM `applications`";
$result = mysqli_query($db, $query);

if($result){
    mysqli_query($db, "UPDATE `applications` SET `status` = 'Решено' WHERE `id` = '$id'");
    unset($_SESSION['message']);
    
    header('Location: /admin/index.php');
} else {
    $_SESSION['message'] = [
        'type' => 'error',
        'text' => 'Ошибка решения заявки'
    ];

    header('Location: /admin/index.php');
}
