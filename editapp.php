<?php 
session_start();

require_once 'vendor/db.php';

if(!isset($_SESSION['user'])) {
    header('Location: /login.php');
}
$id = $_GET['id'];
$query="SELECT * FROM `applications` WHERE `id` = '$id'";
$response = mysqli_query($db, $query);
$app = mysqli_fetch_assoc($response);
?>

<!DOCTYPE html>
<html lang="en">
<head>  
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Нарушениям.нет</title>
    <link rel="stylesheet" href="assets/style/style.css">
</head>
<body>
    <header>
        <div class="logo">
            <div class="logopic"><img src="assets/images/logo.jpg" ></div>
            <div class="logotext"><a href="index.html">Нарушениям.нет</a></div>
        </div>
        <div class="menu">
            <?php 
                if (isset($_SESSION['user'])){
                    if ($_SESSION['user']['level'] == '2'){
                        echo '
                            <div>
                                <a href="admin/index.php"><p>Админ панель</p></a>
                            </div>
                            ';
                    } else {
                        echo '
                            <div>
                                <a href="acc.php"><p>' . $_SESSION['user']['login'] . '</p></a>
                            </div>
                            ';
                    }
                    echo '
                        <div>
                            <a href="vendor/auth/logout.php"><p>Выход</p></a>
                        </div>
                        ';
                }
            ?>
           
        </div>
    </header>
    <main>
        <h2>Создать заявление</h2>
        <center><form action="vendor/user/editapp.php?id=<?php printf($app['id'])?>" method="post">
            <div class="app createapp">
                <div class="apptitle createapptitle">
                    <h3>Номер нарушителя</h3>
                    <input type="text" class="form-input create-input" name="number"required value="<?php printf($app['number']) ?>">
                </div>
                <div class="desc createappdesc">
                    <h3>Описание нарушения</h3>
                    <textarea name="desc" cols="28" rows="14" required maxlength="255" class="form-input textarea"><?php printf($app['description']) ?></textarea>
                </div>
                <button class="appcreate form-btn">Изменить заявление</button>
            </div>
        </form></center>
        <?php

            if (isset($_SESSION['message'])) {
        ?>
            <div class="message <?= $_SESSION['message']['type'] ?>">
                <p class="message-text"><?= $_SESSION['message']['text'] ?></p>
            </div>
        <?php
            }
        ?>
    </main>
    <footer>
        <span>©️ Нарушениям.нет 2024 г. Все права защищены.</span>
    </footer>
</body>
</html>